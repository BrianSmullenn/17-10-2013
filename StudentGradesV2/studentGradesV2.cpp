// Using functions to calculate a student grades

// Include all necessary libraries
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <ios>
#include <iomanip>

//All using methods and types
using std::cout;	using std::cin;		using std::endl;	using std::string;
using std::setprecision;	using std::vector;		using std::streamsize;
using std::sort;			using std::istream;		using std::domain_error;

//All function prototype
double median(vector<double> vec);
double grade(double midterm, double final, double homework);
double grade(double midterm, double final, const vector<double>& hw);
istream& read_hw(istream& in, vector<double>& hw);

int main()
{
	//ask for and read users name
	cout << "Enter your name: " << endl;
	cout << ">";
	string name;
	cin >> name;
	cout << "Hello, " << name << "!" << endl;

	//ask for and read midterm and final grades
	cout << "Enter midterm and final grades: " << endl;
	cout << ">";
	double midtermG, finalG;
	cin >> midtermG >> finalG;

	// ask for homework grades
	cout << "Enter all homework grades: " << endl;
	cout << ">";

	vector<double> homework;
	
	//read homework grades
	read_hw(cin, homework);
	//compute the final grade, if possible
	try
	{
		double overallG = grade(midtermG, finalG, homework);
		streamsize prec = cout.precision();
		cout << "Your final grade is " << setprecision(3) 
			<< overallG << setprecision(prec) << endl; 
	}
	catch(domain_error)
	{
		cout << "You must enter homework grades. Try again!!!!!" << endl;
		return 1;
	}
	system("pause");
	return 0;
}

// Compute the median value of a vector of doubles
// Note: Calling this function copies the entire function

double median(vector<double> vec)
{
	typedef vector<double>::size_type vec_sz;

	vec_sz size = vec.size();
	if(size == 0)
	{
		throw domain_error("Median of an empty string");
	}

	sort(vec.begin(), vec.end());

	vec_sz mid = size/2;

	return size%2 == 0 ? (vec[mid] + vec[mid-1])/2 : vec[mid];
}

//Compute students overall grade
double grade(double midtermG, double finalG, double homeworkG)
{
	return 0.2 * midtermG + 0.4 * finalG + 0.4 * homeworkG;
}

//Compute a students overall grade from midterm, final and vector of hw grades
//Doesnt copy the vector argument
double grade(double midtermG, double finalG, const vector<double>& hw)
{
	if(hw.size() == 0)
	{
		throw domain_error("Student has entered no homework grades");
	}
	return grade(midtermG, finalG, median(hw));
}

//read hw grades from input stream into vector
istream& read_hw(istream& in, vector<double>& hw)
{
	if(in)
	{
		//Get rid of previous contents of homework
		hw.clear();
		//Read in homework grades
		double x;
		while (in >> x)
		{
			hw.push_back(x);
		}

		// Clear the stream so that input will work for next student
		in.clear();
	}
	return in;
}

